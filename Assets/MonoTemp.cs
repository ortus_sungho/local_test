﻿using System;
using UnityEngine;

public class MonoTemp : MonoBehaviour
{
    User ucc = new User();

    // temp
    int m_TempVal = 0;
    private User m_TempUcc = null;

    private void Update()
    {
        //SampleMethodValueType();

        //SampleMethodClassType();

        SampleMethodClassType2();
    }

    private void SampleMethodValueType()
    {
        DateTime startTime = DateTime.Now;
        
        for (int j = 0; j < Const.MaxLoopCount; j++)
        {
            m_TempVal += 1;
        }

        m_TempVal = 0;

        DateTime endTime = DateTime.Now;

        Debug.Log("Record >>> " + (endTime - startTime));
    }

    private void SampleMethodClassType()
    {
        DateTime startTime = DateTime.Now;

        m_TempUcc = ucc;

        for (int j = 0; j < Const.MaxLoopCount; j++)
        {
            m_TempUcc.age += 1;
        }

        m_TempUcc = null;

        DateTime endTime = DateTime.Now;

        Debug.Log("out >>> " + (endTime - startTime));
    }

    private void SampleMethodClassType2()
    {
        DateTime startTime = DateTime.Now;

        m_TempUcc = new User();

        for (int j = 0; j < Const.MaxLoopCount; j++)
        {
            m_TempUcc.age += 1;
        }

        m_TempUcc = null;

        DateTime endTime = DateTime.Now;

        Debug.Log("out >>> " + (endTime - startTime));
    }
}