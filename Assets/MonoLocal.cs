﻿using System;
using UnityEngine;

public class MonoLocal : MonoBehaviour
{
    User ucc = new User();

    private void Update()
    {
        //SampleMethodValueType();

        //SampleMethodClassType();

        SampleMethodClassType2();
    }

    private void SampleMethodValueType()
    {
        DateTime startTime = DateTime.Now;

        int m_TempVal = 0;

        for (int j = 0; j < Const.MaxLoopCount; j++)
        {
            m_TempVal += 1;
        }

        DateTime endTime = DateTime.Now;

        Debug.Log("Record >>> " + (endTime - startTime));
    }

    private void SampleMethodClassType()
    {
        DateTime startTime = DateTime.Now;

        User m_TempUcc = ucc;

        for (int j = 0; j < Const.MaxLoopCount; j++)
        {
            m_TempUcc.age += 1;
        }

        DateTime endTime = DateTime.Now;

        Debug.Log("out >>> " + (endTime - startTime));
    }

    private void SampleMethodClassType2()
    {
        DateTime startTime = DateTime.Now;

        User m_TempUcc = new User();

        for (int j = 0; j < Const.MaxLoopCount; j++)
        {
            m_TempUcc.age += 1;
        }

        DateTime endTime = DateTime.Now;

        Debug.Log("out >>> " + (endTime - startTime));
    }
}